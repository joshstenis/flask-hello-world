## Stage 1: pull official base image
FROM python:3.10-alpine AS build-python

# set working directory
WORKDIR /app

# install psycopg2 (postgres)
RUN apk update \
    && apk add --virtual build-essential gcc python3-dev musl-dev \
    && apk add postgresql-dev
RUN python -m venv /opt/venv

# add venv to PATH
ENV PATH="/opt/venv/bin:$PATH"

# install dependencies
COPY ./requirements.txt .
RUN pip install -r requirements.txt

## Stage 2: pull official base image
FROM python:3.10-alpine

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0
ENV PATH="/opt/venv/bin:$PATH"
COPY --from=build-python /opt/venv /opt/venv

# install psycopg2 (postgres)
RUN apk update \
    && apk add --virtual build-essential gcc python3-dev musl-dev \
    && apk add postgresql-dev
RUN pip install psycopg2

# set working directory
WORKDIR /app

# copy project
COPY . .

# create non-root user
RUN adduser -D myuser
USER myuser

# run gunicorn
CMD gunicorn app:app --bind 0.0.0.0:$PORT